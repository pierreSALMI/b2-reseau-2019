# Procédures

Vous trouverez ici quelques mini-procédures pour réaliser certaines opérations récurrentes. Ce sera évidemment principalement utilisé pour notre cours de réseau, mais peut-être serez-vous amenés à le réutiliser plus tard.  

**Elles sont écrites pour un système Cisco**.

# Sommaire

* [Général](#general)
  * [Les modes du terminal](#les-modes-du-terminal)
  * [Garder ses changements après reboot](#garder-les-changements-après-reboot)
  * [Changer son nom de domaine](#changer-son-nom-de-domaine)
  * [Gérer sa table ARP](#gérer-sa-table-arp)
* [Switches](#switches)
  * [STP](#stp)
  * [CDP](#cdp)
  * [LACP](#lacp)
  * [VLAN](#vlan)
* [Routeurs](#routeurs)
  * [Définir une IP statique](#définir-une-ip-statique)
  * [Ajouter une route statique](#ajouter-une-route-statique)
  * [OSPF](#ospf)

---

## Général

### Les modes du terminal
Le terminal Cisco possède plusieurs modes

Mode | Commande | What ? | Why ?
--- | --- | --- | ---
`user EXEC` | X | C'est le mode par défaut : il permet essentiellement de visualiser des choses, mais peu d'actions à réaliser | Pour visualiser les routes ou les IPs par ex
`privileged EXEC` | enable | Mode privilégié : permet de réalisé des actions privilégiées sur la machine | Peu utilisé dans notre cours au début
`global conf` | conf t | Configuration de la machine | Permet de configurer les interface et le routage 

L'idée globale c'est que pour **faire des choses** on passera en `global conf` pour **faire** des choses, et on restera en **user EXEC** pour **voir** des choses.

### Utiliser le terminal Cisco

L'auto-complétion est votre meilleure amie avec <TAB>. 

Vous pouvez voir les choix possibles avec l'autocomplétion en utilisant `?` à n'importe quel moment dans votre ligne de commande. 

On peut abréger les mots s'il n'y a qu'un seul choix possible : 
```
# show ip interface brief

# show ip int br

Pareil.
```

### Garder les changements après reboot
Les équipements Cisco possèdent deux configurations (d'une certain façon) :
* la `running-config`
  * c'est la conf actuelle
  * elle contient toutes vos modifications
  * `# show running-config` pour la voir
* la `startup-config`
  * c'est la conf qui est chargée au démarrage de la machine
  * elle ne contient aucune de vos modifications
  * `show startup-config`  
  
Comment garder vos changements à travers les reboots ? Il faut copier la `running-config` sur la `startup-config` :
```
# copy running-config startup-config
```

### Changer son nom de domaine
**1. Passer en mode configuration**
```
# conf t
```

**2. Changer le hostname**
```
(config)# hostname <HOSTNAME>
```

### Gérer sa table ARP

* voir sa table ARP
```
# show arp
```

---

## Switches

### Table MAC

* voir sa table MAC
```
# show mac address-table
```

### STP

**0. Voir l'état de STP**
  * sur le matériel Cisco, un arbre STP est créé **par VLAN**
```
# show spanning-tree 
# show spanning-tree bridge
# show spanning-tree summary

# show spanning tree vlan 10 # pour un VLAN spécifique
```

**1. Modifier la priorité d'un switch** (pour un VLAN donné)
  * la priorité **doit** être un multiple de 4096
  * bonne pratique : jamais un switch en priorité 0. Ca permettra d'en mettre un à 0 en cas d'urgence, pour qu'il soit tout de suite élu *root bridge*
  * par défaut, les switches utilisent le VLAN 1
```
# conf t
(config)# spanning-tree vlan X priority 4096 # où X est un numéro de VLAN
(config)# spanning-tree vlan 1 priority 4096 # pour le VLAN 1
```

**🐙2. Configurer des ports *edge***

Activation des fonctionnalités *portfast* et *bpduguard*. 

```
# conf t
(config)# interface ethernet 0/1
(config)# switchport mode access

! portfast mode
(config-if)# spanning-tree portfast

! activation de BPDUguard
(config-if)# spanning-tree bpduguard enable

! désactivation de l'envoi de BPDU
(config-if)# spanning-tree bpdufilter enable
```

### CDP 

**0. Voir l'état de CDP**
```
# show cdp
# show cdp interface
```

**1. Désactiver CDP sur une interface donnée**
```
# conf t
(config)# interface ethernet 0/1
(config-if)# no cdp enable
```

### LACP

**0. Voir l'état des agrégations de ports (*port-channel*)**
```
# show ip int br

# show etherchannel
# show etherchannel summary

# show lacp neigh
```

**1. Configurer deux interfaces avec un *port-channel* LACP**
```
# conf t
(config)# interface range ethernet0/0 - 1

! crée le channel group numéro 1 (en mode active)
(config-if-range)# channel-group 1 mode active

! force l'utilisation du protocole LACP
(config-if-range)# channel-protocol lacp
```

**2. Configurer l'interface *port-channel* comme une autre**
```
# show ip int br
# conf t
(config)# interface port-channel 1

! plus rapide
(config)# interface po1
```

### VLAN

**0. Pour voir les VLANs actuels du switch**
```
# show vlan
# show vlan br
# show interfaces trunk
```

**1. Passer en mode configuration**
```
# conf t
```

**2. Créer le VLAN**
  * le nom est arbitraire, mais c'est une bonne pratique d'en utiliser un
  * un nom clair svp (et le même sur tous les switches d'une même infra pour un même VLAN svp...)
```
(config)# vlan 10
(config-vlan)# name client-network
(config-vlan)# exit
```

**3. Assigner une interface pour donner accès à un VLAN**. C'est le port où sera branché le client. **C'est le mode *access*.**
```
(config)# interface Ethernet 0/0
(config-if)# switchport mode access
(config-if)# switchport access vlan 10
```

**4. Configurer une interface entre deux switches. C'est le mode *trunk*.**
```
(config)# interface Ethernet 0/0
(config-if)# switchport trunk encapsulation dot1q
(config-if)# switchport mode trunk

! pour n'autoriser que les VLAN 10 et 20 sur le trunk
(config-if)# switchport trunk allowed vlan 10,20 
```

---

## Routeurs

### Définir une IP statique
**1. Repérer le nom de l'interface dont on veut changer l'IP**
```
# show ip interface brief
OU
# show ip int br
```
**2. Passer en mode configuration d'interface**
```
# conf t
(config)# interface ethernet <NUMERO>
```
**3. Définir une IP**
```
(config-if)# ip address <IP> <MASK>
Exemple :
(config-if)# ip address 10.5.1.254 255.255.255.0
```
**4. Allumer l'interface**
```
(config-if)# no shut
```
**5. Vérifier l'IP**
```
(config-if)# exit
(config)# exit
# show ip int br
```

---

### Ajouter une route statique

**1. Passer en mode configuration**
```
# conf t
```

**2.1. Ajouter une route vers un réseau**
```
(config)# ip route <REMOTE_NETWORK_ADDRESS> <MASK> <GATEWAY_IP> 
Exemple, pour ajouter une route vers le réseau 10.1.0.0/24 en passant par la passerelle 10.2.0.254
(config)# ip route 10.1.0.0 255.255.255.0 10.2.0.254 
```

**2.2. Ajouter la route par défaut**
```
(config)# ip route 0.0.0.0 0.0.0.0 10.2.0.254 
```

**3. Vérifier la route**
```
(config)# exit
# show ip route
```

### OSPF

**1. Passer en mode configuration**
```
# conf t
```

**2. Activer OSPF**
```
(config)# router ospf 1
```
* le `1` correspond à l'ID de ce processus OSPF
* nous utiliserons toujours `1` pendant nos derniers cours

**3. Définir un `router-id`**
```
# dans nos TPs, le router-id sera toujours le numéro du routeur répété 4 fois
# donc 1.1.1.1 pour router1
(config-router)# router-id 1.1.1.1
```

**4. Partager une ou plusieurs routes**
```
(config-router)# network 10.6.100.0 0.0.0.3 area 0
```
* cette commande partage le réseau `10.6.100.0/30` avec les voisins OSPF, et indique que ce réseau est dans l'`area 0` (l'aire "backbone")
* l'utilisation de cette commande est un peu particulière
* nous ne rentrerons pas dans les détails de fonctionnement (sauf si on a le temps) de OSPF
* **donc retenez simplement que pour le masque, vous devez écrire l'inverse de d'habitude**
* c'est à dire `0.0.0.3` au lieu de `255.255.255.252` par exemple

**Vérifier l'état d'OSPF** :
```
# show ip protocols
# show ip ospf interface
# show ip ospf neigh
# show ip ospf border-routers
```
